<?
require_once './vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader);

$file_content = file_get_contents("./data/data.json");
$data = json_decode($file_content, true);

$file_content = file_get_contents("./data/roles.json");
$roles = json_decode($file_content, true);

echo $twig->render('index.html.twig', ['data' => $data, 'roles' => $roles, 'header' => 'เมนู (Menu)']);